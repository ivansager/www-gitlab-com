---
layout: handbook-page-toc
title: MLOps Single-Engineer Group
---

## Updates
{:.no_toc .hidden-md .hidden-lg}

### [08/Apr/2022] Jupyter Notebook Usage, What's Next?

Toggleable diff modes for Jupyter being released on 14.10, number of comments on Jupyter Notebooks saw a sharp increase this year, a small signals that users are interested on integrating MLFLow into GitLab and using GitLab runners for ML workloads.

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/qOY09Omnmtk" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

### Past

- 30/03/2022 - https://www.youtube.com/embed/_6ljKL6kCRc - Pipelines as part of Create?: High level overview on how pipelines become part of the CREATE stage for MLOps, potential ways we can deliver value to users, and explorations we have been working on.
- 10/03/2022 - https://youtu.be/kBMJKvmqQG4 - General Updates and Introduction to `Pipelines With Stubbed Jobs`, our next thing after Notebook Diffs
- 21/02/2022 - https://youtu.be/MMCV6hsYFms - Introducing CIter, a POC for running pipelines that are not part of the codebase
- 03/02/2022 - https://youtu.be/GjxQLJY2vzs - Overview of Multi-View Diffs (Clear and Raw versions)

[Subscribe to the issue for updates](https://gitlab.com/gitlab-org/incubation-engineering/mlops/meta/-/issues/16)

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## MLOps Single-Engineer Group

DRI: [@eduardobonet](https://gitlab.com/eduardobonet)

MLOps is a [Single-Engineer Group](/company/team/structure/#single-engineer-groups) within our [Incubation Engineering Department](/handbook/engineering/incubation/). This group works on early feature exploration and validation related to the [MLOps group](/direction/modelops/mlops) within the [ModelOps stage](/direction/modelops/).

### Vision

Make GitLab a tool Data Scientists and Machine Learning Engineers love to use.

### Mission

Identify opportunities in our portfolio to explore ways where GitLab can provide a better user experience for Data Science and Machine Learning across the entire Machine Learning life cycle (model creation, testing, deployment, monitoring, and iteration).

### Current Exploration Areas

#### User Personas

Issue: https://gitlab.com/gitlab-org/incubation-engineering/mlops/meta/-/issues/40

Data Scientists and Machine Learning Engineers were not considered part of the target audience of GitLab, and that is evidenced by the lack of User Personas and User Research on these groups. Creating these is essencial to create awareness within the company that these users do indeed exist.

#### Improve User Experience for Jupyter on Code Review

Epic: https://gitlab.com/groups/gitlab-org/-/epics/7194

**Why**

While we delivered a [cleaner experience for Jupyter Diffs](https://gitlab.com/groups/gitlab-org/-/epics/6589), which was really well received by our users, the experience is still very limited. Images within notebooks carry a lot of context, reducing the aplicability of the diffs. Additionally, the raw diff is still imporant in some use cases.

**The Vision**

We want to deliver an experience where Data Scientists can review a Notebook in its full context: the images, the source file, the code blocks, the markdown.

**The Way**

For this, we need to recreate the diff algorithm. Instead of using the markdown transformation, we will split the json notebook into blocks to diffed, which actually makes the diffing process faster, and rely on the cell ids to see which cells were added or removed. The output of this is a renderable block, that is mapped into a line of the source notebook, allowing us to

- Display images
- Render Markdown with equations/images/etc
- Code Highlight
- Comments on blocks of the rendered version, which are mapped into the source. Comments on the source also displayed in the rendered version
- Toggling between raw and rendered diff

#### Can we use GitLab Pipelines for ML?

Epic: https://gitlab.com/groups/gitlab-org/incubation-engineering/mlops/-/epics/5

**The Problem**

Due to the size of the data, ML practitioners have to relly on running computations on the cloud, often through pipelines. However, adding the infra necessary for those pipelines to run can be a job for a full-time engineer. Given that pipelines already enter the MLOps lifecycle at the CREATE stage, the current workflow of depending on a commit to run a new pipeline with small like changes hinder productivity.

**The Vision** 

A Data Scientist working on a new model can test it using GitLab by simply running

```glyter run --repo=my_repo my_notebook.ipynb```

**The Way**

We might be able to do an improved POC here without even changing GitLab codebase:

1. Create the Parent/Child pipeline defition on 
2. Use GitLab API to upload the notebook into the repository
3. Trigger a Parent pipeline with the GitLab API
4. Parent Pipeline downloads the notebook and transforms it into a valid gitlab-ci file
5. Parent pipeline triggers the child pipeline

#### Analytics Repository

Epic: https://gitlab.com/groups/gitlab-org/incubation-engineering/mlops/-/epics/5

**The Problem**

When companies grow, so do their Data Science team. And with this growth, knowledge of past research done within the organization gets lost. This is further exacerbated considering that most of this knowledge resides within Jupyter Notebooks, and forgotten within some lost git repository. The impact is that the same work is repeated every few years.

**The Vision**

The first thing Data Scientists when they start a new task is open GitLab, and go to a component where all notebooks created previously are easily indexed and searcheable.

An example of a open source tool that implements this is https://github.com/airbnb/knowledge-repo

**The Way**

We can test a POC using GitLab Pages and Glyter: a pipeline that indexes and publishes them, with support for tags and pretty display, for both RMarkdown and Jupyter Notebooks, with the limitations that it can't do anything too interactive or take notebook across different repositories. 

#### Can GitLab offer a Model Registry?

> The MLOps SEG will not focus on this area as of now, but it is something that should be heavily considered

One of the early painpoints users have when productizing Machine Learning is how to version the models. Offering a Model Registry through GitLab could be a great addition to our ecossystem. But before that, we first want to help our users connect the tools they already have to Gitlab. A good deliverable would be playbook or catalogue with ready-to-use pipelines that connect to the most popular Model Registries.

Epic: https://gitlab.com/groups/gitlab-org/incubation-engineering/mlops/-/epics/4

### What is MLOps? A Primer

MLOps is the infrastructure needed to create, iterate, deploy and monitor Machine Learning models effectively

While many definitions exists, we will consider here Machine Learning is a set of tools and algorithms that extract information from a dataset to make predictions with a degree of uncertainty. This predictions can be used to enhance user experience or to drive internal decision making.

#### Recommended Resources

- [Machine Learning Engineering](http://mlebook.com/): Great introductory book on all aspects of Machine Learning Engineering
- [Hidden Technical Debt in Machine Learning Systems](https://papers.nips.cc/paper/2015/file/86df7dcfd896fcaf2674f757a2463eba-Paper.pdf): Landmark paper on MLOps
- [MLOps Community](https://mlops.community/): Slack group that brings together people from different backgrounds to discuss ML and MLOps

#### Versioning an ML Model

A machine learning model version is actually result of three different artefacts, and changing any of them can wildly change the output, these artefacts being:

- _Code_

Code refers not only to the algorithm used to train the model (Decision Tree, Neural Networks, etc), but also the process of pulling the necessary data to make predictions and the code that transforms this data into a format that can be consumed by the model (feature transformations). It is imperative that the feature transformation code is exactly the same when training the model and deploying the model

- _Hyperparameters_

These are the parameters passed to the model to change its behaviour and to the feature transformers to get the data into the right shape. Some examples of hyperparameters are the seed used for RNG, the max depth of a Decision Tree, the maximum value for a specific features. This numbers can be hyper parameters are sometimes obtained manually to see what works best, and sometimes in automated ways such as Grid Search. You could also see this as configuration	 parameters.

- _Training Dataset_

The dataset used to generate the model is as important as the code, and the same model with the exact same hyper parameters can have a very different output if trained on different datasets. It is good practice to store the datasets that were used for every model version, which can create problems with large duplicated datasets. Using immutable changelogs as the data source for model training make this process a lot simpler, since one just needs to record the id of the last row used.

It is also important to store the data set because one of the basic assumptions for an ML model to work is that future or unseen samples are generally similar than the samples on the training data. Keeping a copy of the dataset enables monitoring of possible divergences (See Concept Drift)

#### Architectural Patterns

Although still in its early stages, the MLOps community has been discovering a few patterns that are common when creating an architecture for Machine Learning. This document will describe some of this patterns briefly and create a vocabulary.

We will often use the word `store` from now on, and it merits a definition. A store is a service that allows for an artefact to be stored, searched, versioned, collaborated on and even monitored . It’s a service within the organisation that centralizes evolution and usage of a category of artefacts. In contrast, a `registry` only performs storage and search. So a `Model Store` contains a `Model Registry`, but not the other way around.

##### Pipeline Runner

Epic: https://gitlab.com/groups/gitlab-org/incubation-engineering/mlops/-/epics/5
Label: ~incubation::mlops::pipelines

_Underlying problem_

1. Training a model takes too long, and needs to be repeated many times
2. Convention laptops don't always provide the hardware necessary for running ML code
3. Training needs data that doesn't fit into the laptop

_Solution_

For ML, pipelines are necessary much earlier on the development of a model than regular software development. While models can be trained locally (and often are), having access to an easy-to-use pipeline runner quickly pays off in productivity. Some solutions, like KubeFlow Pipelines, provide features that are attractive specifically to ML practitioners (converting a jupyter notebook into a pipeline for example).

Note that the pipeline runner doesn't necessarily need to be Machine Learning specific. AirFlow is often used here, since it's common for companies to already provide it internally, and even GitLab is already applied on this scenario.

_Examples_

- [AirFlow](https://airflow.apache.org/)
- [KubeFlow Pipelines](https://www.kubeflow.org/docs/components/pipelines/overview/pipelines-overview/)
- [AWS Sagemaker Pipelines](https://aws.amazon.com/sagemaker/pipelines/)

##### Model Registry/Store

Epic: https://gitlab.com/groups/gitlab-org/incubation-engineering/mlops/-/epics/4
Label: ~incubation::mlops::modelregistry

_Underlying problem_

1. I have a few models in production, but I find it hard to iterate on them
1. My team has trouble identifying all the models available in the company, and we are doing duplicated work
1. I have trouble managing which models are in production
1. I have issues on deploying the right version correctly
1. I don't know if the models I put in production have the expected result

_Solution_

Model Store (or Model Registry, or Model Metadata store) is a component on the MLOps architecture aiming to make it easier to version, discover and iterate on models. Often this component also logs production metrics for the models (input, resources, data drift, etc)

_Examples_

- [MLFlow](https://mlflow.org/)
- [Neptune.ai](https://neptune.ai/)
- [AWS SageMaker Model Registry](https://docs.aws.amazon.com/sagemaker/latest/dg/model-registry.html)
- [Iterative.ai Studio](https://studio.iterative.ai/)

##### Feature Store

_Underlying problem_

- Once the number of models start to grow, so does the number of features and the complexity of transforming those features.
- If I have two or three models that use the same feature with the same transformation, each model would have to contain the logic to transform that feature
- For large organizations, different deparments might be creating and using the same features without realizing
- Keeping the same feature definition for both training and serving avoids Concept Drift down the road
- Data changes quickly, specially on models that are served through streaming

_Solution_

Feature Store centralizes both how a feature is accessed, but also how it is transformed, and how it is accessed. This way, when creating a new model, Data Scientists can search for already existing features and build upon them, or create a new one with a library of operators. Instead of connecting to the data lake directly, during training and serving all features are retrieved directly from the Feature Store.

A feature store can also be seen as the connection point between MLOps and DataOps.

_Examples_

- [Tecton.ai](https://www.tecton.ai/)
- [Feast](https://feast.dev/) OpenSource, is now part of Kubeflow, and supported by Google as part of Vertex.ai


Issue Link
[https://gitlab.com/gitlab-org/gitlab/-/issues/329591](https://gitlab.com/gitlab-org/gitlab/-/issues/329591)

### Hystorical Internal Discussions

- [ModelOps Discussion w/ Board Members](https://docs.google.com/document/d/1ONYRRw7tSOpGcETiM2J_P-Df58MLi3PKGRXfl5iIYL8/edit)
- [GitLab ModelOps](https://docs.google.com/document/d/1jI1z3aT8kpF20vlxKeTteowVGfFJEh3BmnzG-cjtnZU/edit?ts=60538b97)
- [MLOps with Gitlab Paper](https://docs.google.com/document/d/1uMmB8gad7fe28aZCSBOIK6z9CbCtDmFV2ZUUuHH1YcY/edit#)
- [AI/ML Customers at Gitlab](https://docs.google.com/document/d/1QuR-kdFHYbb7HiBpJ36U3znd70juFkduVXVVY3YNd5A/edit)
- [https://datastudio.google.com/u/0/reporting/c708a721-2e95-409b-9a40-4c14eff1ae06/page/4OrWB](https://datastudio.google.com/u/0/reporting/c708a721-2e95-409b-9a40-4c14eff1ae06/page/4OrWB)

### Industry news

- [6 May 2021 - Introducing GitLab, Bitbucket Cloud, and Bitbucket Server source code management for Algorithmia](https://algorithmia.com/blog/introducing-gitlab-bitbucket-cloud-and-bitbucket-server-source-code-management-for-algorithmia)
- [31 Aug 2021 - DataBricks announce 1.6b Series H funding](https://sprou.tt/1lE9mOagwDU)
