---
layout: handbook-page-toc
title: "Digital Marketing Management"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Digital Marketing Managers
{: #digital-marketing-management}
<!-- DO NOT CHANGE THIS ANCHOR -->

### Goals and objectives accomplished with digital campaigns:
{: #goals-objectives}
<!-- DO NOT CHANGE THIS ANCHOR -->

Generating INQs (Inquiries) that lead to MQLs (Marketing Qualified Leads) and SAOs (Sales Accepted Opportunities), and helping improve our conversion rate of INQ > MQL and MQL > SAO

We also support these objectives (ordered from top to bottom funnel):
- Brand Awareness
- Website Traffic
- Event Registrations
- Webcast Views
- Content Downloads
- Lead Generation

## DMP labels in GitLab
{: #labels}
<!-- DO NOT CHANGE THIS ANCHOR -->
*Note*: Some of the following labels only exist on the [Digital Marketing project](https://gitlab.com/gitlab-com/marketing/demand-generation/digital-marketing) level.

* **Digital Marketing**: General label to track all issues related to Digital Marketing
* **SEM**: Used for issues that require organic and paid search initiatives
* **Paid Ads**: Used for any paid advertising campaign such as Google Ads
* **Paid Social**: Used for paid social campaigns such as LinkedIn and Facebook Ads 
* **mktg-demandgen** - Demand Gen general, add since we are part of the Demand Gen team
* **mktg-digitalmarketing** - Digital Marketing general, issues specific to Digital Marketing only

## DMP Slack channels
{: #slack}
<!-- DO NOT CHANGE THIS ANCHOR -->
*  `#digital-marketing`: General digital marketing conversation and questions

## Paid Digital Marketing 
{: #paid-digital-marketing}
<!-- DO NOT CHANGE THIS ANCHOR -->

### How does paid digital contribute to GitLab’s funnel?
{: #funnel}
<!-- DO NOT CHANGE THIS ANCHOR -->
- Top of Funnel (Awareness): Introduce GitLab brand to potential customers with awareness and reach objectives.
- Middle of Funnel (Consideration): Nurturing engaged prospects with educational, assets, consideration-stage content and events.
- Bottom of Funnel (Purchase): Leading prospects to conversion by retargeting with relevant, personalized ad experiences.

[Click here](/handbook/marketing/demand-generation/digital-marketing/digital-marketing-management/#channel-definitions) to see channel definitions and where they fit in buyer journey. 

The Digital Marketing team’s strategy & execution ultimately leads to filling pipeline, while closely analyzing down funnel metrics & results in order to influence & optimize top of funnel strategy in a feedback loop.

### Working with PMG, our Digital agency
{: #agency}
<!-- DO NOT CHANGE THIS ANCHOR -->
Who is PMG Digital?

All paid digital campaigns are executed with the help of our digital agency PMG (learn more about PMG [here](https://www.pmg.com/)). PMG fees are a percentage of the paid digital spend, so total budgets will include the effective spend with a PMG fee. The only exception is account-centric Paid Display which is handled directly through [Demandbase](https://about.gitlab.com/handbook/marketing/revenue-marketing/account-based-strategy/demandbase/), but PMG still handles regular Paid Display with GDN (Google Display Network) and DV360 (Google Display and Video 360). For GitLab digital initiatives, PMG provides:


* Strategy & planning - Covering full-funnel paid strategy both holistically and granularly, considering the GitLab brand and audience for all channels and tactics.
* Buying & executing - Gathering all necessary campaign materials (goals & objectives, creative, copy, targeting, etc) to launch, and strategically bidding/negotiating for ad units/packages and optimizing for more efficient costs towards goals throughout campaigns.
* Insights & reporting - Aggregating all digital performance into custom report dashboards, offering actionable insights for audience, asset, regional, and  campaign performance within each issue, and reviewing overall performance over time to uncover trends & opportunities.
* Billing - Handling all paid digital spend directly with publishers & platforms, then syncing with GitLab finance to manage invoicing.

GitLab Digital Marketing works directly with PMG within the GitLab [Digital Advertising project](https://gitlab.com/gitlab-com/marketing/digital-advertising), which is strictly for Digital Marketing & PMG communication only. To learn how PMG is involved in the paid digital request process, please refer to the [DMP Request Workflow](/handbook/marketing/demand-generation/digital-marketing/digital-marketing-management/#workflows) section below.

### Digital Campaign Channels
{: #campaign-channels}
<!-- DO NOT CHANGE THIS ANCHOR -->
Digital Marketing will recommend specific channels based on your campaign goals. The most common type is Paid Social based on robust targeting criteria and successful performance in reach and inquiry volume.

#### Channels
{: #channels}
<!-- DO NOT CHANGE THIS ANCHOR -->

* Paid Social
   - LinkedIn  - Sponsored Content Ads, Carousel Ads, InMail/Message Ads, and Conversation Ads
   - Facebook - Single Image & Video Ads, Lead Generation Ads, Carousel Ads
   - Instagram - Single Image & Video Ads, Lead Generation Ads, Carousel Ads
   - Twitter - Promoted Tweets, Image or Video
* Paid Search
   - Google - Expanded Text Ads, Responsive Search Ads
   - Bing - Expanded Text Ads, Responsive Search Ads
* Paid Display
   - GDN (Google Display Network) - Display Image Ads, Responsive Display Ads
   - DV360 (Google Display & Video 360) - Programmatic Display Image Ads, Responsive Display Ads, Access to private ad networks
   - Demandbase is a digital marketing platform for Display only, mainly used to target specific accounts. Please note that Demandbase is managed by the ABM team, partnering with Demandbase directly.  Please contact the ABM team (focuses on Tier 1,2,3 accounts) and Field Marketing team (focuses on named accounts) if you would like to create a Demandbase campaign, or check out the [DemandBase page](/handbook/marketing/revenue-marketing/account-based-strategy/demandbase/) for more info.
* Sponsorships (Publisher Engagements): 
   - Participating in Virtual Event (Virtual Conference, Panel, Talking Head, All-Day Event/Summit)
   - Third Party Custom Webcasts (single & multi-vendor)
   - Newsletter ads
   - Custom email blasts
   - Sponsored Custom & 3rd Party Content Creation (trend reports, ebooks, articles, etc.)
   - General third party event sponsorships
   - Podcasts
   - Site Display Ads
   - Microsites
   - Content Syndication


### Channel definitions
{: #channel-definitions}
<!-- DO NOT CHANGE THIS ANCHOR -->

#### **Paid Social**:
{: #paid-social}
<!-- DO NOT CHANGE THIS ANCHOR -->
Paid social ads are sponsored ads that we show on social platforms. The three social media platforms that we primarily advertise on are Facebook/Instagram, LinkedIn (including LinkedIn InMail & Conversation Ads), and Twitter.
* **Best used for**: Top and mid funnel content. Does well for both awareness and direct response (depending on the asset used). LinkedIn InMail and Conversation ads stand out as more mid and bottom funnel.
     * **Best type of content to use**: Live webcasts, recorded webcasts, events, and ebooks/guides. Live events with a sign-up deadline, tend to perform the best in paid social. 
     * **Worst type of content to use**: Thought leadership reports/whitepapers like Gartner and Forrester reports.
* **Types of targeting we do**:
     * **Prospecting targeting**: Sponsored ads to audiences that are built based on personas using interest, professional, and demographic targeting. A profile is developed based on people who convert. We would then show ads to people natively within either Facebook, Instagram or LinkedIn that closely match that profile. This method helps drive new traffic to GitLab that may not know about the brand or product. 
     * **Account targeting**: Sponsored ads on targeting speciific accounts or contacts based on a list upload or import from Marketo. LinkedIn is the top channel for this type of marketing based on a +90% account match rate, whereas Facebook and Instagram requires accounts to be manually entered in order to target.
     * **Retargeting**: Show  ads in order to re-engage people who have already visited pages on the GitLab website.

#### **Paid Search**: 
{: #paid-search}
<!-- DO NOT CHANGE THIS ANCHOR -->
Paid search promotes [text ads](https://support.google.com/google-ads/answer/1704389?hl=en) on Google and Bing search engines to drive people to specific GitLab landing pages as they are looking for information on search engines. We do this by [bidding](https://support.google.com/google-ads/answer/2459326?hl=en) on targeted keywords and phrases based on the assumed intent of the person, triggering our text ad relevant to a specific user search query, and leading users to a related landing page that matches that intent.
* **Best used for**: Mid and bottom funnel content where we want someone to take action (ex: filling out a form to a gated asset, signing up for a demo, etc).
     * **Best type of content to use**: Use case type gated assets that directly applies to the intent for the search query. How-to guides and ebooks do well here.
     * **Worst type of content to use**: Events and thought leadership reports/whitepapers like Gartner and Forrester reports that are not specifically tied to the intent of a search query. Caveat here is if the content is more of a how to report or guide.
     * **Targeting**: Paid Search is neither able to target by company size/segments nor by domain or company name. This channel is best used for brand campaigns.

#### **Paid Display**: 
{: #paid-display}
<!-- DO NOT CHANGE THIS ANCHOR -->
Display ads are banner ads that we mostly run are through the [Google Display Network](https://support.google.com/google-ads/answer/2404190?hl=en) (GDN) and [Display & Video 360](https://support.google.com/displayvideo/answer/2949929?hl=en) (DV360). GDN will show banner ads on websites that have [Google Adsense](https://support.google.com/adsense/answer/6242051?hl=en) set up on their website. There are no specific websites we show banner ads on - we earn the ad space by bidding on placements based on specific targeting criteria such as demographics, topics, and interests. DV360 will show banner ads on websites from ad exchanges outside of Google’s network, using more first and third party data. On occasion, we run banner ads on specific websites through direct buys, but this is handled more in the publisher program.
* **Best used for**: Top and mid funnel content. More used for awareness and some action-based response.
     * **Best type of content to use**: How-to guides, ebooks, and general awareness content promotion do well here. Trial ads tend to do well if used in some form of remarketing strategy.
     * **Worst type of content to use**: Events and thought leadership reports/whitepapers like Gartner and Forrester reports. Because there is a learning period when running Display advertising, running events through Display advertising is not recommended due to the short promotion period. With leadership reports/whitepapers, we historically have not seen good engagement and sign-ups for these types of assets on Display advertising. 
* **Types of targeting we do**:
     * **Contextual targeting**: Show banners ads on websites that are related to the content of our landing page and website. This is done based on keyword and topic targeting.
     * **Prospecting targeting**: Show banner ads to related audiences that are similar to those who have converted on our website. A profile is developed based on people who convert. We would then show banner ads to people that closely match that profile. This method helps drive new traffic to GitLab that may not know about the brand or product. 
     * **Retargeting**: Show banner ads in order to re-engage people who have already visited pages on the GitLab website. This tactic can show ads on what seems to be irrelevant websites. However, targeting is based on the engagement of the person, not the context of a website. 

#### **Publisher Sponsorships**:
{: #publisher-sponsorships}
<!-- DO NOT CHANGE THIS ANCHOR -->
Publisher sponsorships are when we engage a specific publisher in order to purchase placement on their web properties. Generally, we make sure the publisher's website(s) and audience closely match the profile of who we want to advertise to before engaging with the publisher. Sponsorships are less turn key than our other programs - while they require longer planning and execution, heavier lifts in content creation, and manual reporting, the long investment results in more qualified leads that are aligned with our goals & target audience.
* **Best used for**: Primarily used for demand generation, so we focus on mid to bottom funnel content.
     * **Best type of content to use**: Live webcasts and recorded webcasts work the best. Ebooks and guides sometimes work, depending on the placement. 
     * **Worst type of content to use**: Events and thought leadership reports/whitepapers like Gartner and Forrester reports. With leadership reports/whitepapers, we historically have not seen good engagement and sign-ups for these types of assets on Paid Social, unless the report is more of a how-to guide. 

[Steps to set up Content Syndication in Marketo & SFDC](https://about.gitlab.com/handbook/marketing/marketing-operations/campaigns-and-programs/#steps-to-setup-content-syndication-in-marketo-and-sfdc).


### Digital Campaign Ad Specs
{: #specs}
<!-- DO NOT CHANGE THIS ANCHOR -->
Each paid channel has its own unique design specifications, ad copy character limits, and recommendations for their ad types to ensure ads can run at their optimal performance. If you do not yet have creative assets secured for your campaign, the design team can use this section as their guide when producing your creative.

#### Paid Social
{: #social-specs}
<!-- DO NOT CHANGE THIS ANCHOR -->
* Facebook Image:
   - Recommended Image Size: 1080x1080 pixels, 1200x628 pixels
   - Recommended Image Ratio: 1:1, 1.91:1 to 4:5
   - Recommended Image File Type: JPG or PNG
   - Copy: Primary Text: 125 character limit
   - Copy: Headline: 40 character limit
   - Copy: Description: 30 character limit
* Facebook Video:
   - Recommended Video Ratio: 9:16 (full vertical) to 16:9 (feed/landscape)
   - Recommended Video File Type: MP4 or MOV
   - Recommended Video File Size: 4GB Max
   - Recommended Video Length: 5-15 seconds
   - Video Captions: Optional but recommended
   - Video Sound: Optional but recommended
   - Copy: Primary Text: 125 character limit
   - Copy: Headline: 40 character limit
   - Copy: Description: 30 character limit
* Facebook Carousel
   - Recommended Resolution: 1080x1080 pixels
   - Recommended Image File Type: JPG or PNG
   - Recommended Video File Type: MP4, MOV or GIF
   - Recommended Ratio: 1:1
   - Number of Carousel Cards: 2 to 10
   - Image Maximum File Size: 30MB
   - Video Maximum File Size: 4GB
   - Video Duration: 1 second to 240 minutes
   - Aspect Ratio Tolerance: 3%
   - Copy: Primary Text: 125 character limit
   - Copy: Headline: 40 character limit
   - Copy: Description: 20 character limit
* Instagram Image
   - Recommended Resolution: 1080x1080 pixels
   - Recommended Image File Type: JPG or PNG
   - Recommended Ratio: 1:1
   - Image Maximum File Size: 30MB
   - Copy: Primary Text: 125 character limit
   - Copy: Headline: 40 character limit
* Instagram Video:
   - Recommended Video Ratio: 1.91:1 to 4:5 supported
   - Recommended Resolution: 1080x1080 pixels
   - Recommended Video File Type: MP4, MOV or GIF
   - Recommended Video File Size: 30MB Max
   - Recommended Video Length: 1 second to 2 minutes
   - Video Captions: Optional but recommended
   - Video Sound: Optional but recommended
   - Copy: Primary Text: 125 character limit
   - Copy: Headline: 40 character limit
* Instagram Carousel
   - Recommended Resolution: 1080x1080 pixels
   - Recommended Image File Type: JPG or PNG
   - Recommended Video File Type: MP4, MOV or GIF
   - Recommended Ratio: 1:1
   - Number of Carousel Cards: 2 to 10
   - Image Maximum File Size: 30MB
   - Video Maximum File Size: 4GB
   - Video Duration: 1 second to 60 minutes
   - Aspect Ratio Tolerance: 1%
   - Copy: Primary Text: 125 character limit
* LinkedIn Image (Sponsored Content):
   - Recommended Image Size: 1200x627 pixels
   - Recommended Image Ratio: 1.91:1
   - Recommended Image File Type: JPG or PNG
   - Recommended Image File Size: 5MB Max
   - Recommend using shorter copy within image, and lean on the headline/introduction text to convey more of your message
   - Copy: Primary Text: 150 character limit
   - Copy: Headline: 70 character limit
   - Copy: Description: 100 character limit
* LinkedIn Video: 
   - Recommended Video Length: Less than 15 seconds
   - Recommended Video File Size: 75 KB to 200 MB
   - Recommended Video File Format: MP4
* LinkedIn Carousel
   - Recommended Resolution: 1080x1080 pixels
   - Recommended Image File Type: JPG or PNG
   - Recommended Ratio: 1:1
   - Number of Carousel Cards: 2 to 10
   - Image Maximum File Size: 10MB
   - Video not supported currently
   - Copy: Primary Text: 150 character limit
   - Copy: 45-character limit for carousel ads that direct to a destination URL
   - Copy: 30-character limit for carousel ads with a Lead Gen Form CTA
* LinkedIn InMail
   - Recommended Resolution: 300x250 pixels
   - Recommended Image File Type: JPG, GIF (non-animated), or PNG (no flash)
   - Image Maximum File Size: 40KB
   - Copy: Message Subject: 60 characters limit
   - Copy: Message text: 1,500 character limit
   - Copy: Hyperlinked text in message: 70 character limit
   - Copy: CTA button copy: 20 character limit
* Twitter Image: 
   - Recommended Image Size: 1200x675 pixels
   - Recommended Image Ratio: 1:1
   - Recommend using shorter copy within image, and lean on the headline/introduction text to convey more of your message
   - Copy: Primary Text: 280 character limit
   - Copy: Headline: 70 character limit
* Twitter Video:
   - Recommended Video Size: 1200x1200
   - Recommended Video Ratio: 1:1
   - Recommended Video Length: less than 15 seconds
   - Recommended Branding: Consistent in upper left-hand corner
   - Recommended Video File Type: MP4 or MOV
   - Recommended Video File Size: 1GB Max
   - Copy: Primary Text: 280 character limit
   - Copy: Headline: 70 character limit

#### Paid Display (Google Display Network)
{: #display-specs}
<!-- DO NOT CHANGE THIS ANCHOR -->
* Image Sizes
   - 160x600
   - 250x250
   - 300x1050
   - 300x250
   - 300x600
   - 320x50
   - 336x280
   - 728x90
   - 970x250
* Recommended Image File Type: JPG or PNG
* Recommended Image File Size: 150KB Max
* In-Banner Design High Performers
   - Benefit/Value Prop and educational messaging
   - CICD emblem background
   - “Learn More” CTA
   - 4-7 word volume

#### Paid Search
{: #search-specs}
<!-- DO NOT CHANGE THIS ANCHOR -->
- Copy: Description: 90 character limit
- Copy: Headline: 30 character limit
- Copy: Display URL path: 15 character limit

## Requesting Digital Marketing Promotions
{: #requests}
<!-- DO NOT CHANGE THIS ANCHOR -->
If you would like to request a paid digital marketing promotion in paid search, paid social, paid sponsorships or other paid marketing to support your event, content marketing, webcast, asset, etc. create an issue in the [Digital Marketing Repo](https://gitlab.com/gitlab-com/marketing/demand-generation/digital-marketing/-/tree/master) and then follow the [Paid Digital Request template](https://gitlab.com/gitlab-com/marketing/demand-generation/digital-marketing/-/blob/master/.gitlab/issue_templates/paid-digital-request.md). If you are specifically requesting a Demandbase campaign, create an issue with the [Demandbase Campaign Request template](https://gitlab.com/gitlab-com/marketing/demand-generation/digital-marketing/-/blob/master/.gitlab/issue_templates/Demandbase_Campaign_Request.md).

### Issue Items Defined
{: #issue-items}
<!-- DO NOT CHANGE THIS ANCHOR -->
* **Campaign Name**: Name of the campaign that you are requesting, or the Integrated / Use Case campaign this new initiative falls under
* **Campaign Budget**: How much budget you would like to allot to this campaign
* **Purchase Order Number: From Coupa, please provide the PO number that is generated from your approved requisition 
* **Campaign/Finance Tag**: If you have a specific Campaign/Finance Tag that you need us to bill to, please input here. These will also be used as the `utm_campaign` name in reporting (minus spaces, underscores, and special characters)
* **Team Making Promotion**: Team that we should bill to (i.e. Field Marketing - AMER, Corporate Marketing, etc). 
* **Campaign Description**: Brief description of the campaign and what you are trying to achieve
* **Campaign Start and End Dates**: Dates of when the campaign should start and end
* **Campaign Goal**: What is the numeric goal and KPI for the campaign (registrations, page views, downloads, etc)
* **Campaign Target Audience**: The type of people that you want to reach in this campaign
* **Campaign Creative Asset**: If you have a creative set already for this campaign, please provide links to the creative. If not, this may require a separate ask for Design team, please make request in Corporate project and factor into timeline. Digital Marketing iterates on designs in Canva for testing & variations, so you also have the option of providing a design that you’d like to iterate on.

### Purchase Orders Required for Campaign Requests
{: #po-process}
<!-- DO NOT CHANGE THIS ANCHOR -->
Because GitLab is now a publicly traded company, our finances are required to be [Sarbanes-Oxley compliant](https://about.gitlab.com/handbook/internal-audit/sarbanes-oxley/). In order to be compliant, each campaign request will need to have an associated purchase order to the campaign prior to launching the campaign. Below is the following workflow in order to obtain a purchase order number and documentation needed to be compliant:

1. Open a [Campaign Request issue](https://about.gitlab.com/handbook/marketing/demand-generation/digital-marketing/digital-marketing-management/#requests) and request that Digital Marketing team obtain a statement of work from our digital advertising agency. Note that Campaign Request template will need to be fully filled out with detail in order for a statement of work to be requested
1. When statement of work is created, Digital Marketing team will post statement of work back in the Campaign Request issue so campaign requester can start the process of creating a [Coupa requistion](https://about.gitlab.com/handbook/business-technology/enterprise-applications/guides/coupa-guide/#how-to-create-a-requisition)
1. Once the Coupa requistition is approved, a PO will be created. Campaign requester will then update the Campaign Request template with the appropriate purchase order number
1. Digital Marketing team will then submit a campaign request with our digital advertising agency to start to exectute the campaign
1. Digital Marketing team will make sure the digital advertising agency includes the following information on the invoice for billing in the notes of the invoice:
     - Purchase order number
     - Allocadia ID
     - Flight dates of the campaign
     - Team to be billed

### DMP Request Workflows
{: #workflows}
<!-- DO NOT CHANGE THIS ANCHOR -->
**1. Paid Ads  (Search, Social, Display)** 
* Before creating an issue in Digital Marketing, this information is required:
   * Budget
   * Start and end dates
   * General targeting parameters (target buyer/user persona, job title, industry, etc)
   * Target account list (if applicable)
   * Geo targeting within certain regions
   * Landing Page URL(s)
   * Campaign/Finance tag
   * If you would like guidance from the DMP around certain items, please make a comment within the issue.
* Assign to both Matt Nguyen and Leslie Stinson
* Digital Marketing will then create an issue for our digital agency PMG.
   * Only Digital Marketing can create issues within Digital Advertising (our project for PMG).
   * Digital Marketing will liaise between the campaign owner and the agency within separate issues so wires are not crossed.
   * Please do not comment in PMG issues within Digital Advertising unless you are added as a participant.
* Digital Marketing or Campaign Manager will create a design request issue if no creative is provided.
   * Please specify if you'd like to iterate on an existing design for Digital Marketing to execute on in Canva.
* Digital Marketing will approve copy & mockups.
   * PMG will generate copy & CTAs based on the landing page or content brief.
   * Digital Marketing will approve copy and mockups, unless approval from the campaign owner is requested.
* If testing is requested, Digital Marketing will secure testing variations (copy, CTA, and/or creative) and propose a schedule for testing rounds.
* PMG will launch campaign and Digital Marketing will confirm launch within the issue.
* Digital Marketing will provide campaign summary and report(s) after the campaign ends.

**2. Publisher Engagements (Content Syndication, Hosted Webinars, Lead Generation Packages, and Sponsorships)** 
* For each publisher engagement, the requester must submit an issue with information around the campaign’s goals (target segment, persona, region, lead cost, etc) so PMG can generate an RFP covering 3-5 partners with programs that align with those goals. Some program types include:
   * Sponsorships: ad placement information, including creative specs
   * Content Syndication: content to promote (PDF file recommended)
   * Lead Generation Packages: lead list information and upload & implementation strategy 
   * Hosted Webinar: webinar information, event date, presentation assets
   * If you would like guidance from Digital Marketing around certain items, please make a comment within the issue.
* Assign to Matt Nguyen and Leslie Stinson.
* Digital Marketing will then create an issue for our digital agency PMG.
   * Only Digital Marketing can create issues within Digital Advertising (our project for PMG).
   * Digital Marketing will liaise between the campaign owner and the agency within separate issues so wires are not crossed.
* Digital Marketing or Campaign Manager will create a design request issue if creative is needed for this campaign.
   * Please specify if you'd like to iterate on an existing design for Digital Marketing to execute on in Canva.
* Digital Marketing will approve copy & mockups if needed for this campaign.
   * PMG will generate copy & CTAs based on the landing page or content brief (if applicable)
   * Digital Marketing will approve copy and mockups, unless approval from the campaign owner is requested.
* Digital Marketing will confirm launch within the issue.
* Digital Marketing will set up tracking & nurture with the Campaign Manager, and provide the sponsorship program’s lead list to Marketing Ops to upload.
* Reporting will be provided via Publisher, and PMG can also provide Publisher Reporting through Google Data Studio.

### DMP Request Timing
{: #timing}
<!-- DO NOT CHANGE THIS ANCHOR -->
* Time required to get a campaign into market
   * Ideally, we need at least 3-4 weeks before the proposed launch date in order to plan strategy, forecast goals, and secure all creative assets.
* Turnaround time for mock-ups (if applicable)
   * Once creative & copy are generated, PMG can produce ad mockups for review upon request.
* Turnaround time to populate audiences (if applicable)
   * If the campaign is targeting a named account list, it can take 24-48 hours to populate within a platform.
* Turnaround time for Design
   * Depending on the request size, the campaign owner’s request for new designs from the brand design team  could take at least 5-10 business days. 
   * Digital Marketing can iterate on designs within Canva, so this option can reduce turnaround time if examples and copy is provided early.
   * A request for 2 LinkedIn images will take much less time than a request for a fully integrated campaign that requires multiple display & paid social sizes.
* Timing required for publisher engagements (sponsorships)
   * This will depend primarily on the publisher response time. Once PMG is connected with the publisher contact, 
* Expectations for reporting
   * Current DataStudio report(s) - request from Digital Marketing

### Campaign Duration & Learning Phase
{: #campaign-duration}
<!-- DO NOT CHANGE THIS ANCHOR -->
We require campaigns to run at least 3-4 weeks (this is typically for event/webcasts promotions) in order to successfully exit the initial “learning phase” for our platforms’ algorithms. The learning phase is the period when the ad delivery system still has a lot to learn about a new campaign. During the learning phase, the delivery system is exploring the best way to deliver your ads - this means performance is less stable and costs are usually worse. Each time an ad is shown, the delivery system learns more about the best people and places to show the ad. The more an ad is shown, the better the delivery system becomes at optimizing the ad’s performance.

### Campaign Request Requirements & Best Practices
{: #requirements-best-practices}
<!-- DO NOT CHANGE THIS ANCHOR -->

- Field Campaigns: In alignment with the FY22 contract (which can be found in ContractWorks) GitLab has with our digital vendor PMG, paid campaign promotion for Revenue Marketing is limited to 3 campaign launches per month.
   * Please check this [PMG Revenue Marketing Campaign Tracker](https://docs.google.com/spreadsheets/d/1lylnQb3wXq1ry2n_ifqxtzKX13KaFtVTVzbWn7jDkbI/edit?usp=sharing) to confirm availability before requesting a paid campaign.
   * If you have a campaign that lasts multiple months, that campaign only counts against RevMktg's 3 in that 1st month and does not count against the 3 in the subsequent months. 
- Landing Pages: All landing pages should be GitLab-owned (about.gitlab, learn.gitlab, and page.gitlab) so we are able to track conversion/inquiry performance. Conversion data increases the success of campaigns by advancing the platform learning phase and providing PMG with valuable data & insights to make manual optimizations. 
   * 3rd party landing pages are not able to report on conversions within our platforms, so they can significantly decrease campaign performance. Furthermore, if paid campaigns are not able to attribute their conversions/inquiries from 3rd party landing pages, reporting will lack correct data and show major inefficiencies. 
   * If you are planning on using a 3rd party landing page, please [open an issue in the  Digital Marketing project](https://gitlab.com/gitlab-com/marketing/demand-generation/digital-marketing/-/issues/new?issuable_template=paid-digital-request) before a contract is signed so we can determine how to capture conversions and measure success.
- Localization/Translation: While our ads are mainly in English, we are able to run campaigns in different languages but recommend all materials are translated. This means the creative copy, ad copy, landing page, and nurture have a fully localized, seamless experience from end to end.
- Lead Time + Flight Time: We recommend followng our [Request Timing section](/handbook/marketing/demand-generation/digital-marketing/digital-marketing-management/#timing) to ensure your campaign is set up for success.
- Ad Creative: We recommend adding a CTA (call to action) button, and using shorter copy within the image (typically 4-7 words) and leaning on the headline/introduction text to convey more of your message.

## LinkedIn Campaigns
{: #linkedin}
<!-- DO NOT CHANGE THIS ANCHOR -->
LinkedIn is one of our top channels based on targeting capabilities, lead quality and efficient costs across regions & segments. Due to the success in this channel, we have expanded our presence and now utilize certain ad units and features that are specific to LinkedIn advertising only.

### LinkedIn Lead Gen Forms
{: #linkedin-lgf}
<!-- DO NOT CHANGE THIS ANCHOR -->
The implementation of Lead Gen Forms (LGF) in our LinkedIn sponsored ads has increased our CPI efficiency and reach in the channel. LGFs are native forms that come pre-filled with accurate LinkedIn profile data, and the single-click submit reduces the number of steps before conversion, taking users directly to their requested content. Digital Marketing uses LGFs for LinkedIn campaigns that promote ungated assets in Pathfactory. While this works for Demand Gen campaigns, it will not work for other goals such as event registration or 3rd party page registration that require on-site form fills.

Since inquiries are submitted in-app within our LinkedIn ads and not via GitLab owned landing pages, these inquiries are considered offline touchpoints and do not generate the same Bizible touchpoint data as normal inquiries. 

### LinkedIn Targeting
{: #linkedin-targeting}
<!-- DO NOT CHANGE THIS ANCHOR -->
LinkedIn features a powerful selection of targeting criteria that aligns with Demand Gen’s segmentation and GTM plans.

Matched Audiences - covering both company and individual contact lists, Matched Audiences is the most effective tool in reaching specific accounts & contacts through a list upload (typically exported from SFDC or Marketo). Our LinkedIn advertising account usually hits a 90%+ match rate, so ads can reach most users within our target lists. Please note that uploading a new audience list can take 24-48 hours to populate.
Firmographic Targeting - using first party & up-to-date firmographic data, we can target audiences based on company size, industry, job titles, skills, seniority, etc. This targeting feature helps us segment based on buyer/user personas and sales segments.
LinkedIn Groups & Interests - this type of targeting reaches members who share interests or professional associations by belonging to the same LinkedIn Group, and member interest topics based on actions and engagement with specific content on LinkedIn.

### InMail & Conversation Ad Campaigns
{: #inmail-conversation-campaigns}
<!-- DO NOT CHANGE THIS ANCHOR -->
#### InMail (Message Ads)
{: #inmail}
<!-- DO NOT CHANGE THIS ANCHOR -->
LinkedIn InMail Ads (also known as Message Ads) are static messages delivered directly to LinkedIn member inboxes, creating a personalized ad experience between the user and GitLab. These ads are highly effective for middle-funnel campaigns, targeting specific individuals with custom tailored content relevant to their persona and/or organization. Since many users also have LinkedIn notify their personal or work email address when they have received an InMail, this results in up to 2x higher open rates and click throughs than traditional single email campaigns. Unlike our main paid digital bid type of costs per click, we pay for the message open which generally costs around $1 - $1.50 each. However, when a user shares the ad to other users, those users' opens and clicks are free. As a result, the free sharing feature allows our campaigns to obtain greater reach at a lower cost. This is especially useful in Account Based Marketing campaigns; a manager receiving an InMail to attend an upcoming GitLab webinar can share with other users in their organization. InMail/Message Ads can be used to re-engage and nurture accounts with upcoming events, new content assets, or by simply opening the conversation. 

#### Conversation Ads
{: #conversation-ads}
<!-- DO NOT CHANGE THIS ANCHOR -->
Conversation Ads are similar to InMail Ads in terms of placement, costs, and targeting, but instead of a static message, the user is able to engage through a sequence of messages prompted by specific CTA buttons, creating an interactive experience with GitLab. Conversation Ads are delivered directly to the LinkedIn member’s inbox, starting off with an intro message offering a variety of CTA buttons and proceeding through a custom sequence with each button click, resulting in several layers of interactions. LinkedIn reporting features visualization into engagement through the message flow, highlighting which messages & CTAs users found most relevant and where conversation drops off. These CTAs can click through to the next message  (“Tell me more”), click out to a relevant landing page (“See case studies”), click out to an event registration page (“Register now”), or open an lead generation form (“Sign up now”). Conversation Ads are essentially a Choose Your Own Adventure interaction that provide target audiences with more options and provide us with more audience insights.

If you are requesting InMail or Conversation Ads, create an issue in the general [Paid Digital Request template](https://gitlab.com/gitlab-com/marketing/demand-generation/digital-marketing/-/blob/master/.gitlab/issue_templates/paid-digital-request.md).


#### Sender Collaboration
{: #inmail-collaboration}
<!-- DO NOT CHANGE THIS ANCHOR -->
InMail & Conversation Ad campaigns must have a designated sender in order to send messages on behalf of GitLab, putting a friendly face to the company name. The ads displayed in a member’s inbox will appear to be sent from the designated sender, which allows for increased personalization and message relevance to targeted members.

In order for our digital agency PMG to set up sender permissions, they’ll need to send requests to those whom we want to grant sender permissions, which are typically SDRs but can include a variety of titles & teams at GitLab. Since sender requests can only be sent to 1-degree connections, a member of the PMG team will first send a LinkedIn connection request, then the sender request once the connection is approved. When the designated team member receives a request to be a sender on a LinkedIn Messaging campaign, they can approve or reject this request by following the link provided in the email. The GitLab team member can also [use this link](https://www.linkedin.com/ad/accounts?destination=sponsored-inmail-sender-permissions) to manage their sender permissions and see whether they have sender access for a campaign. Once the GitLab team member has accepted their sender permissions to be added as a sender on the account, they will need to notify the DMP by confirming within the campaign issue.

#### Copy
{: #inmail-conversation-copy}
<!-- DO NOT CHANGE THIS ANCHOR -->

Although Digital Marketing can generate copy, we highly recommend that the sender, another member of the sender’s team, or a PMM generate the copy to be used in messages since they have more insight & context around targeted accounts, user/buyer persona, and/or the promoted content. When creating an InMail or Conversation Ad campaign, the Ad Copy Template doc (included for both ad types in the [Paid Digital Request template](https://gitlab.com/gitlab-com/marketing/demand-generation/digital-marketing/-/blob/master/.gitlab/issue_templates/paid-digital-request.md) will be cloned for your specific campaign and used as a collaborative space for the sender to generate messaging and other campaign members to edit where necessary. Conversation Ad copy will be more complex, as it includes several layers of messages and multiple CTA options for each layer. Please note that due to the high volume of messages within a given campaign, LinkedIn restricts replies from the target audience to the sender. If the sender prefers replies, they can include a link to their Calendly or email address in the message. One of the key benefits of InMail & Conversation Ads is that you can personalize copy with macros. Macros allow you to pull a member’s LinkedIn profile data and insert them dynamically into your ad for more personalization. Profile data options include first name, last name, job title, company name, and industry.

**InMail Copy Best Practices:**
* Customize the greeting with the member’s name 
* Refer to their job title  
* Try using the word “you"
* The copy is fewer than 1,000 characters
* The CTA is clear
* The landing page is optimized for mobile

Best performing subject lines often use some of the following keywords:
* Thanks 
* Exclusive invitation  
* Connect 
* Job opportunities  
* Join us  

Additional ways that we can test with message tone of voice:
* Genuine
* The Helpful Advisor
* VIP Invitation - sending personalized invites to 'exclusive' events
* The Cliffhanger

**Conversation Ad Copy Best Practices:**
* Always start with your main goal and/or objective: What is the ideal action you want the member to take when they receive your Conversation Ad? 
* To drive brand consideration: Link to your blog posts, pre-recorded webinars, or industry trends and analysis
* To drive leads and turn prospects into customers: Share product demos or tutorials, customer success stories, or invite prospects to attend an event
* Introduce yourself: When using an individual as a sender, use the opening message to introduce yourself and let members know why you’re reaching out
* There’s no subject line for Conversation Ads. Like any other LinkedIn message, the first sentence will appear as the subject. Because your audience will see this in their LinkedIn Messaging, make your first sentence count. 
* Use multiple messages & buttons: Your conversation should have 2 to 5 layers. Each layer will consist of message text and at least 2 CTA buttons with responses to the question in your message. For instance, if your goal is to drive content downloads, share two pieces of content that your audience can choose from. 
* Don’t include “Not Interested” or “No Thanks” CTAs: Avoid including this in the first layer of your conversation. Prospects who aren’t interested will just close the ad. Instead, focus on using CTAs they can learn from.
* Ask your email marketing team for ideas: Check in with your email marketing team to understand what works best for them. Use their top performing email copy as inspiration for your Conversation Ad. 
* Keep copy short & sweet: Conversation Ads are meant to feel more like a live conversation. Keep your messages short and friendly, and don’t forget about the character limits. Consider using language that is more casual, the way you would over the phone.
* Stick to a conversational tone: While you may be engaging a B2B audience, remember that it’s people who make the decisions to buy your product or service, and they want to know that your brand is human and authentic.


## Reporting
{: #reporting}
<!-- DO NOT CHANGE THIS ANCHOR -->
### Reporting Metrics Defined
{: #reporting-metrics}
<!-- DO NOT CHANGE THIS ANCHOR -->
Front End:
- Spend: Total effective spend (not including PMG fee)
- Clicks: Total ad clicks
- Impressions: Total ad views served
- CTR: Click-through rate (clicks divided by Impressions)
- CPM: Average cost per 1,000 impressions (typically in Display campaigns only)
- CPC: Cost per click (cost divided by clicks)
- Inquiries (INQ) (commonly known as conversions): Form fills on the campaign landing page + form fills tagged with the campaign utms anywhere on our marketing site.
- CPI: Cost per inquiry
- CVR: Conversion rate (inquiries divided by clicks)

Back End:
- MQL: Marketing qualified lead (number of campaign inquiries that have MQL’ed)
- Cost per MQL: Cost of MQLs generated from campaign (spend divided by MQLs)
- SDR Accepted: Number of campaign inquiries worked by the SDRs
- SAO: Sales accepted opportunity (number of MQLs that have converted to SAO)
- Cost per SAO: Cost of SAOs generated from campaign (spend divided by SAOs)
- INQ to MQL rate: percentage of leads that convert from Inquiry to MQL (MQLs divided by INQs)
- MQL to SAO rate: percentage of leads that convert from MQL to SAO (SAOs divided by MQLs)

### Digital Marketing Reports
{: #digital-marketing-reports}
<!-- DO NOT CHANGE THIS ANCHOR -->
#### Sisense Reports
{: #sisense-reports}
<!-- DO NOT CHANGE THIS ANCHOR -->
**[Demand Gen Dashboard in Sisense](https://app.periscopedata.com/app/gitlab/793304/WIP:-Demand-Gen-Dashboard):** This dashboard provides an overview of demand gen campaigns performance, measured by our key performance indicators (KPIs): Inquiries, MQLs, and SDR Accepted. These metrics are pulled using Bizible touchpoints data, while Opportunities, Total IACV$, SAO, Pipeline IACV$, Won Deals count, and Won IACV$ metrics are pulled using the Linear Bizible Attribution Touchpoints model.

#### Data Studio reports
{: #data-studio-reports}
<!-- DO NOT CHANGE THIS ANCHOR -->
**[PMG Digital Campaign Report](https://datastudio.google.com/s/kIiVtvUZyWQ):** This Data Studio report features all front end digital marketing performance through PMG and can be drilled down to audience, content, region, etc. Only back end performance shown covers Inquiries (total inquiries, cost per inquiry (CPI), and inquiry conversion rates). This report can be filtered by:
- Integrated / Use Case Campaign name (UTM campaign name)
- Content (UTM content)
- Geo (Regions and sub-regions)
- Targeting
   - Prospecting (pr)
   - Target Account List (abm)
   - Retargeting (rtg)
   - Target Account List Retargeting (abm-rtg)
   - Retargeting with Marketo Smart Lists (rtg-marketo)
- Medium (UTM medium)
   - Paidsocial
   - Cpc
   - Display
   - Sponsorship
- Source (UTM source)
   - Google
   - LinkedIn
   - Twitter
   - Facebook
   - DV360
- Content Type
   - Webcast
   - Gated download
   - eBook
   - Free trial
   - Demo

**[PMG Field Marketing Campaign Report](https://datastudio.google.com/s/vHd-LcgPH5E):** For Field Campaigns only, can be filtered by:
- Campaign Code (UTM campaign name, typically campaign finance tag)
- Content (UTM content)
- Geo (Regions and sub-regions)
- Targeting
   - Prospecting (pr)
   - Target Account List (abm)
   - Target Account List Retargeting (abm-rtg)
- Medium (UTM medium)
   - Paidsocial
   - Sponsorship
- Source (UTM source)
   - LinkedIn
   - Facebook
   - Sponsor Name

#### SFDC reports
{: #salesforce-reports}
<!-- DO NOT CHANGE THIS ANCHOR -->
- **[FMM - WW FMM Digital Campaign Leads report](https://gitlab.my.salesforce.com/00O4M000004aA0V):** Provides overview of field campaign inquiries with lead status and touchpoint data
- **[LinkedIn Lead Gen Forms](https://gitlab.my.salesforce.com/00O4M000004aXN1):** Provides overview of lead gen form (LGF) campaign inquiries


## UTMs for URL tagging and tracking
{: #utm-tracking}
<!-- DO NOT CHANGE THIS ANCHOR -->
All URLs that are promoted on external sites and through email must use UTM URL tagging to increase the data cleanliness in Google Analytics and ensure marketing campaigns are correctly attributed. 

We don't use UTMs for internal links. UTM data sets attribution for visitors, so if we use UTMs on internal links it resets everything when the clicked URL loads. This breaks reporting for paid advertising and organic visitors.

You can access our internal [URL tagging tool in Google Sheets](https://docs.google.com/spreadsheets/d/12jm8q13e3-JNDbJ5-DBJbSAGprLamrilWIBka875gDI/edit#gid=0). You will also find details in this spreadsheet on what "Campaign Medium" to use for each URL. If you need a new campaign medium, please check with the Digital Marketing Programs team as new mediums will not automatically be attributed correctly.

If you are not sure if a link needs a UTM, please speak with the marketer who is managing your campaign to ensure you are not interrupting the reporting structure they have in place.

UTM construction best practices:
- lowercase only, not camelcase
- alphanumeric characters only
- no spaces
- **Campaign Medium** covers general buckets like `paidsearch`, `social`, or `sponsorship` 
- **Campaign Source** names where the link lives. Examples include `ebook`, `twitter`, or `qrcode`
- **Campaign Name** describes a specific campaign. Try to add additional context like `reinvent`, `forrester`, and `bugbounty`
- **Campaign Content** differentiates ad types.
- **Campaign Term** identifies keywords used in a campaign


## Opt-Out of Seeing GitLab Digital Ads
{: #opt-out-ads}
<!-- DO NOT CHANGE THIS ANCHOR -->

We run digital ads on the following channels:

* Google (paid search and display)
* Facebook
* LinkedIn
* Twitter
* Demandbase (paid display)

Because everyone at GitLab works remotely, it makes it difficult to restrict ads from being shown to GitLab team members. In a brick and mortar environment, we can easily block IP addresses to accomplish this. Because everyone at GitLab has a different IP address, and even dynamic IP addresses, there is no way to implement an exclusion rule that would block all GitLab ads to GitLab team members. 

If you would not like to see GitLab ads, you are able to opt-out of ads as you see them. Below is the process to remove GitLab ads. Please note that if you use both your personal and work accounts on your devices, you will need to exclude from both your personal and work accounts. 

**Google Paid Search**: When you see a GitLab text ad in Google search engine results
1. On the ad, click on the arrow next to the URL
2. Click on Why this ad? 
3. On the toggle next to “Show ads from gitlab.com”, toggle off
4. Close out of box

**Google Display**: When you see a GitLab ad on a third-party website
1. Click on the blue arrow icon at the top right-hand corner for the ad 
2. Select “Stop seeing this ad” when the option appears
3. Make a selection on why you do not want to see the ad anymore
4. Done

**Facebook**: When you see a GitLab ad on Facebook
1. On the upper right-hand corner of the ad, click the three dots in that corner
2. When, menu appears, click on “Why am I seeing this ad?”
3. In the next pop-up box, under “What You Can Do”, click the “Hide” button next to the option that says “Hide all ads from this advertiser”

**LinkedIn**: When you see a GitLab ad on LinkedIn
1. On the upper right-hand corner of the ad, click the three dots in that corner
2. When option box appears, click on “Report this ad, I don’t want to see this ad in my feed”
3. In the pop-up box that appears next, select an option
4. Click the Submit button on the next page

**Twitter**: When you see a GitLab ad on Twitter
1. On the upper right-hand corner of the ad, click the arrow
2. When option box appears, click on “I don’t like this ad”
* Note that this does not necessarily block all ads, just that specific ad. You would need to do this on all the ads you see from GitLab
* You could block @gitlab in your profile to no longer see ads. However you block all ability to engage with the @gitlab Twitter accounts as well (seeing Tweets, Retweeting, mentioning, etc). 


