---
layout: handbook-page-toc
title: Content in Campaigns
description: Everything you need to know about how we leverage content in marketing campaigns, including ungated content journeys and former gated landing page processes.
twitter_image: /images/tweets/handbook-marketing.png
twitter_site: '@gitlab'
twitter_creator: '@gitlab'
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview
{: #overview .gitlab-purple}
<!-- DO NOT CHANGE THIS ANCHOR -->

This page documents ways in which content is leveraged in campaigns, including in-house created content, analyst relations content, and downloadable competitive content.

In an effort to best engage our technical audience, we are pursuing an **ungated content journey** that leverages Pathfactory tracks, rather than gated landing pages. This allows viewers to preview content before signing up to view in its entirety. We have flexibility about when and how the forms display, and will test to deliver the best experience with proven results. This process will also be more efficient to launch new content so time can be focused on how to leverage the content in campaigns.

The epic code in each section below outlines the necessary issues to open in order to "check off" the list of action items to launch a new piece of content. *While this page exists within the Campaigns Team handbook page, it is meant to be contributed to by all teams in marketing. If you see an updated needed, please submit an MR and assign to `@jgragnola`.*

* Jump to ungated content journey (Pathfactory) setup process
* Jump to gated content (Landing Page) setup process - *being phased out*

### Types of content in campaigns
{: #content-types .gitlab-orange}
<!-- DO NOT CHANGE THIS ANCHOR -->
* **Content for use in marketing campaigns:** we leverage the content in our marketing channels (website, email nurture, paid digital, organic social, etc.)
    * [Internal GitLab-created content](/handbook/marketing/demand-generation/campaigns/content-in-campaigns#internal-content): We created and developed the content in house
    * [External content (i.e. Analyst Relations)](/handbook/marketing/demand-generation/campaigns/content-in-campaigns#external-content): We have bought the rights to use the content from an external vendor (analysts or publishers, for example) or received the content from a partner
    * [On-Demand Webcasts](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/webcast/)
* **[Content syndication](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/gated-content/content-syndication)** (under Digital Marketing): We have promoted our content through a third-party vendor, but do not drive people back to our website. In these cases, we often have given them the resource to make available for download to their audience, and recieve the leads to be uploaded.

## How to pick content for campaigns
{: #picking-content-for-campaigns .gitlab-purple}
<!-- DO NOT CHANGE THIS ANCHOR -->
**Search the entire Pathfactory Content Library**

https://gitlab.lookbookhq.com/authoring/content-library/content

Filter by one or multiple of the following:
- Topic
- Content Type
- Funnel Stage
- GTM Motion (Business Unit in Pathfactory)
- Language

**View reports to see what works**

* **Key metric to analyze: Engagement Time**
   - *"Why not total views?"* Engagement time is a better indicator of content effectiveness than views, which can be a self-fulfilling prophesy; the more views, the more it is used, the more it continues to climb in comparison to other content. 

## Internal Content (i.e. eBooks, Guides)
{: #internal-content .gitlab-purple}
<!-- DO NOT CHANGE THIS ANCHOR -->
The below process should be used for all new content (ebooks, guides, whitepapers, etc.). Creating the epic and related issues is the responsibility of the **Content Owner** (i.e. Content Marketing, Analyst Relations) to make sure that when the content is ready, the teams involved in putting into Pathfactory (MOps) and the teams involved in activating (Campaigns, Digital, ABM) are able to take action immediately.
#### Epic code and issues - Internal GitLab Content
{: #epic-issues-internal-content}
<!-- DO NOT CHANGE THIS ANCHOR -->

🏷️ **Label statuses:**
* **status:plan**: content is in a brainstormed status, not yet approved and no DRI assigned
* **status:wip**: content is approved, with launch date determined and DRI assigned

1. **Internal Content Epics:** `Content Marketing` creates epic (using code below) and associates to campaign epic
1. **Related Issues:** `Content Marketing` creates the issues as designated in the epic code, and associates to the content epic

As a guide in developing timeline, please view the workback timeline calculator [here](https://docs.google.com/spreadsheets/d/1RvYLEUJvh9QZJZX1uXiKJPcOOSGd7dEeNneqyA2bt3A/edit#gid=969067029)

```
<!-- NAME EPIC: [type] <Name of Asset> (ex. [eBook] A beginner's guide to GitOps) -->

## Launch date: `to be added`

## Landing Page: (add when live)

## Pathfactory link: (add when live)

#### :key: Key Details
* **Content Marketing DRI:**  
* **Official Content Name:** 
* **Official Content Type:** 
* **Primary Campaign:** 
* **Primary Sales Segment:**
   - [ ] Enterprise
   - [ ] Mid-Market
   - [ ] SMB
* **Primary Buying Stage:**
   - [ ] Awareness
   - [ ] Consideration
   - [ ] Decision/Purchase
* **Primary Persona:** 
* **Language:** 
* **Marketo Program Name:** `YYYY_Type_NameAsset` <!-- as content owner, you make this up. Follow structure, no spaces, keep it short - i.e. `2020_eBook_BegGuideGitOps`. This will be used for MKTO/SFDC program. -->
* [ ] [main salesforce program]() - `to be created`
* [ ] [main marketo campaign]() - `to be created`
* [ ] [asset copy draft]() - `doc to be added by Content Marketing`
* [ ] [pathfactory track link]() - `link to PF track (the track in PF, not the live link) when created`


### :books:  Issues - Content Owner to Create

[Use the workback timeline calculator to assign correct due dates based off of launch date](https://docs.google.com/spreadsheets/d/1RvYLEUJvh9QZJZX1uXiKJPcOOSGd7dEeNneqyA2bt3A/edit#gid=969067029)

**Required Issues:**
* [ ] :calendar: Not an issue, but an action item for content owner: Add to [SSoT Marketing Calendar](https://docs.google.com/spreadsheets/d/1ni6gKeWhjtrNppMdYvPESsCRjDbfVdYjTNtUtcNBFGg/edit#gid=571560493)
* [ ] [Asset Copy Issue](https://gitlab.com/gitlab-com/marketing/inbound-marketing/global-content/content-marketing/-/issues/new?issuable_template=content-resource-request) - Content
* [ ] [Landing Page Issue](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-content-landing-page) - Content & Campaigns
* [ ] [Nurture Emaily Issue](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=add-to-intelligent-email-nurture) - Content, Lifecycle & Campaigns
* [ ] [Asset Design Issue](https://gitlab.com/gitlab-com/marketing/inbound-marketing/global-content/content-marketing/-/issues/new?issuable_template=design-request-content-resource) - Digital Design
* [ ] 🧨 Not an issue, but an action item for content owner: upload to Pathfactory
* [ ] [Pathfactory Track Issue](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-pathfactory-track) - Campaigns
* [ ] [Resource Page Addition Issue](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-resource-page-addition) - Campaigns

<details>
<summary>Expand below for quick links to optional activation issues to be created and linked to the epic.</summary>

* [ ] [Digital Marketing Promotion Issue](https://gitlab.com/gitlab-com/marketing/demand-generation/digital-marketing/-/issues/new?issuable_template=paid-digital-request) - Digital Marketing
* [ ] [Organic Social Issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/new?issuable_template=social-gtm-organic) - Social
* [ ] [Homepage Feature (only when applicable)](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/new?issuable_template=request-website-homepage-promotion) - Growth Marketing
* [ ] [Blog](https://about.gitlab.com/handbook/marketing/blog/) - Editorial
* [ ] [PR Announcement Issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/issues/new?issuable_template=announcement) - PR

</details>

/label ~"Content Marketing" ~"Gated Content" ~"mktg-demandgen" ~"dg-campaigns" ~"mktg-status::wip"
```

## External Content (i.e. Analyst Relations)
{: #external-content .gitlab-purple}
<!-- DO NOT CHANGE THIS ANCHOR -->
External content can be sourced from Analyst Relations, partners, and other vendors with whom we work. Some examples would be vendor comparisons (i.e. Gartner/Forrester) or industry/market analyses (i.e. DevOps Institute).

When a GitLab team member (i.e. AR) determines that they will be purchasing or recieving content to be used in campaigns, they are responsible for working with the [GTM Motion teams](/handbook/marketing/plan-fy22/#core-teams) to discuss how to leverage the content, as well as creating the epics and associated issues to request work of all relevant teams (outlined below to try to make it efficient, comprehensive, and repeatable!).

All external content should be planned in advance of purchase with `at least a 30 business day time to launch date`. This allows time to plan activation into existing and future integrated campaigns and GTM Motions.

#### Epic code and issues - External Content
{: #epic-issues-external-content}
<!-- DO NOT CHANGE THIS ANCHOR -->

🏷️ **Label statuses:**
* **status:plan**: content is in discussion status, not yet approved/purchased
* **status:wip**: content is approved/purchased, with launch/delivery date determined and DRI assigned

1. **External Content Epics:** `Content Owner (i.e. AR, Digital, etc.)` creates epic (using code below) and associates to primary GTM Motion epic
1. **Related Issues:** `Content Owner (i.e. AR, Digital, etc.)` creates the issues as designated in the epic code, and associates to the external content epic

As a guide in developing timeline, please view the workback timeline calculator [here](https://docs.google.com/spreadsheets/d/1RvYLEUJvh9QZJZX1uXiKJPcOOSGd7dEeNneqyA2bt3A/edit#gid=969067029)

```
<!-- NAME EPIC: [type] <Name of Asset> (ex. [Report] Gartner MQ for ARO) -->

* [ ] Make sure epic is CONFIDENTIAL, if applicable (i.e. Analyst Reports)

## Launch date: `to be added`

## [Pathfactory link]() - `to be added when live`

#### :key: Key Details
* **External Content (i.e. AR) DRI:**  
* **Official Content Name:** 
* **Official Content Type:** 
* **Primary Campaign:** 
* **Primary Sales Segment:**
   - [ ] Enterprise
   - [ ] Mid-Market
   - [ ] SMB
* **Primary Buying Stage:**
   - [ ] Awareness
   - [ ] Consideration
   - [ ] Decision/Purchase
* **Primary Persona:** 
* **Language:** 
* **Budget:** <!-- Match to Allocadia -->
* **Marketo program name:** `YYYY_Type_VendorNameAsset` <!-- as content owner, you make this up. Follow structure, no spaces, keep it short - i.e. `2020_report_GartnerMQARO`. This will be used for MKTO/SFDC program. -->
* [ ] [main salesforce program]() - `to be added`
* [ ] [main marketo campaign]() - `to be added`
* [ ] [pathfactory track link]() - `link to PF track (the track in PF, not the live link) when created`

### [Pathfactory & Resource Page Copy]() - `doc to be added by Content Owner` ([use template here](https://docs.google.com/spreadsheets/d/1dKVIZGbbOLoR5BdCqXqCQ40qJlQNif9waTiHc8yWggQ/edit#gid=1837173931)

### :books:  Issues to Create

[Use the workback timeline calculator to assign correct due dates based off of launch date](https://docs.google.com/spreadsheets/d/1RvYLEUJvh9QZJZX1uXiKJPcOOSGd7dEeNneqyA2bt3A/edit#gid=969067029)

**Required Issues:**
* [ ] :calendar: Not an issue, but an action item for content owner: Add to [SSoT Marketing Calendar](https://docs.google.com/spreadsheets/d/1ni6gKeWhjtrNppMdYvPESsCRjDbfVdYjTNtUtcNBFGg/edit#gid=571560493)
* [ ] [Landing Page Issue](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-content-landing-page) - Content & Campaigns
* [ ] [Nurture Emaily Issue](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=add-to-intelligent-email-nurture) - Content, Lifecycle & Campaigns
* [ ] 🧨 Not an issue, but an action item for content owner: upload to Pathfactory
* [ ] [Pathfactory Track Issue](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-pathfactory-track) - Campaigns
* [ ] [Resource Page Addition Issue](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-resource-page-addition) - Campaigns

<details>
<summary>Expand below for quick links to optional activation issues to be created and linked to the epic.</summary>

* [ ] [Analyst Report Commentary Page Issue](https://gitlab.com/gitlab-com/marketing/strategic-marketing/product-marketing/-/issues/new?issuable_template=AR-Commentary-Page) - Analyst Relations
* [ ] [Digital Marketing Promotion Issue](https://gitlab.com/gitlab-com/marketing/demand-generation/digital-marketing/-/issues/new?issuable_template=paid-digital-request) - Digital Marketing
* [ ] [Organic Social Issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/issues/new?issuable_template=social-general-request) - Social
* [ ] [Homepage Feature (only when applicable)](https://gitlab.com/gitlab-com/marketing/indbound-marketing/growth/-/issues/new?issuable_template=request-website-homepage-promotion) - Growth Marketing
* [ ] [Blog Issue](https://gitlab.com/gitlab-com/marketing/inbound-marketing/global-content/content-marketing/issues/new#?issuable_template=blog-post-pitch) - Editorial
* [ ] [PR Announcement Issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/issues/new?issuable_template=announcement) - PR
* [ ] Notify Sales Comms about new report available - AR

</details>

/label ~"Analyst Relations" ~"Gated Content" ~"mktg-demandgen" ~"dg-campaigns" ~"mktg-status::wip"
```
