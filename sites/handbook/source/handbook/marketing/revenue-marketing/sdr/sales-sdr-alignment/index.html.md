---
layout: handbook-page-toc
title: "SDR & Sales Alignment"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Purpose / Objective

The purpose of the **Global Sales-SDR Alignment Framework** is to provide an outbound guideline that helps identify goals, provide better alignment, and highlight expectations for the following roles:

1. Marketing: [Sales Development Representative](https://about.gitlab.com/job-families/marketing/sales-development-representative/) (SDR)
1. Sales: [Strategic Account Leader](https://about.gitlab.com/job-families/sales/strategic-account-leader/) (SAL)
1. Sales: [Account Executive](https://about.gitlab.com/job-families/sales/account-executive/) (AE)
1. Sales: [Inside Sales Representative](https://about.gitlab.com/job-families/sales/public-sector-inside-account-representative/) (ISR)

The alignment framework is designed to facilitate a successful relationship between SDRs and Sales ensuring both are properly positioned to achieve their goals. 

```mermaid 
graph LR
subgraph Marketing Division
    A[SDR]
end
subgraph Sales Division
    A <--> B[SAL]
    A <--> C[AE]
    A <--> D[ISR]
end
```

Please see the [Sales Development](/handbook/marketing/revenue-marketing/sdr/) Handbook Page and [Sales](/handbook/sales/) Handbook Page for additional details.

## Goals / Targets 

### Criteria for Sales Accepted Opportunity

Note: see [GTM Resources](/handbook/sales/field-operations/gtm-resources/) for key Marketing and Sales terms, the Customer Lifcycle, and related Marketing & Sales activities.

1. Quarterly Targets: SDRs have Quarterly Targets based on the following criteria:
   - **Enterprise SDRs** (Sales Segment = Large and Named) have Quarterly targets composed of SAOs, Qualified Meetings and IACV Pipeline Contribution
   - **Commercial MM SDRs and Commerciam SMB SDRs** (Sales Segment = Mid-Market &/or SMB) have quarterly targets composed of SAOs and IACV Pipeline Contribution
   - **SMB and PubSec SDRs** have monthly SAO targets
   - **Ramping SDRs** of all types have a target of 0 in their first month (onboarding/building foundation), 50% month 2 and then 100% in month 3 (considered fully ramped)
1. See [SDR Compensation and Quota](/handbook/marketing/revenue-marketing/sdr/#sdr-compensation-and-quota) for more details around SDR targets.
1. Sales Accepted Orders: SAOs will originate from both inbound and outbound leads and activities.
SDRs are required to qualify inbound leads and outbound contacts, leverage [Force Management Command of Message](https://about.gitlab.com/handbook/sales/command-of-the-message/) principles, create Initial Qualifying Meetings (IQMs), update Salesforce (SFDC) with all notes/next steps and introduce the Sales into opportunities.

#### Parent Account Merging

When and if an opportunity is accepted and moved to SAO, but later needs to be merged to a larger parent opportunity within an account, the Sales AE should follow these steps:
1. Move Opportunity to Stage 8-Closed Lost
1. Select Closed Lost Reason as Merged into another opportunity and select Save
1. Once saved, copy and past the parent opportunity (one that it was merged into) into the Closed Lost Details field on the opportunity

## SDR & Sales Alignment

The [Territories Mapping File](https://docs.google.com/spreadsheets/d/1iTDCaHN-i_xrfiv_Tkg27lYbZ3LHsERySkvv4cPsSNo/edit#gid=720021722) is the Single Source of Truth for ownership by segments, territories, and teams. All teams should use this SSOT to ensure alignment on who is responsible for what.

### Expectations and Strategic Processes

#### Weekly SDR-Sales Sync Meeting

This weekly meeting is critical to drive and ensure alignment between teams.
- Historically, our SDR/Sales pairings who have incorporated this meeting into their week have been the most successful at driving new pipeline.
- In the first sync, SDR and SAL create working agreement and share expectations and team goals.
- Frequency: Weekly
- Duration: 30 min - 1 hour
- Purpose: Strategize on account plans for outbound prospecting, review lists of key personas, discuss messaging approach, as well as discuss inbound traffic, review inbound lead flow, etc.
- Agenda:
     1. SDR Inbound lead flow review (MQL, Qualifying and Inquiry fields) and Outbound ICP’s actioned and Outreach sequence review. What traction has been made at prioritized accounts? Any roadblocks?
     1. Sales review personal traction at prioritized accounts, review existing Opps, roadblocks, etc.
     1. Discuss any upcoming IQMs and IQMs that should be SAOs but are still in “pending” status
     1. Discuss any SAOs that have gone dark (Sales has made at least 6 attempts to contact: 3 calls/3 emails)
     1. Field & Corporate Marketing Issue Board review/plan
     1. Items to review/follow up on at next sync (SDR and Sales action items)

For the first initial team meeting, create a [working agreement](https://docs.google.com/document/) and weekly agenda outline that the SDR and Sales can agree upon. 
     1. Best Practice: SDR and Sales connect 10min pre-IQM to review notes and connect post IQM for a brief retro to discuss notes, follow-up and SAO status.
     1. Highly Encouraged, but not required is for SDR or Sales to create a Slack channel and invite the SDR, Sales Executive, Area Sales Manager, SDR Manager, Solutions Architect, and Technical Account Manager. The purpose of this channel is to have high-level communication regarding leads and opportunities as well as discuss questions. 
     1. Please use chatter in SFDC to communicate changes and requests on existing opportunities. 

#### Process Workflow

1. SDR to nurture and qualify all inbound MQLs in their territory. Discuss lead flow with your Sales Executive.
1. SDR to prospect into target accounts and provide Sales with updates/traction. SDR should share lists of key personas (ICP’s) being inputted into Outreach for prospecting.
1. Sales should provide feedback on outbound Outreach.io sequences that SDR creates.
     1. Please note: SDRs do not create quotes, directly manage accounts and should not get stuck on a small list of accounts for too long.
1. SDR to seek guidance and direction from Sales in regards to both inbound and outbound leads that the SDR is qualifying.
     1. Review and apply CoM principles 
     1. A/B Testing in Outreach sequences
1. SDR to review regional field marketing campaign board with Sales to form a strategy on how to best leverage recent and upcoming events.
     1. [Field Marketing worldwide view](https://gitlab.com/groups/gitlab-com/marketing/-/boards/915336): to find your regional view, please use the menu bar in the upper left corner of the board view
     1. [SDR: Field Marketing Event Outreach & Follow Up](https://gitlab.com/gitlab-com/marketing/sdr/issues/197#trophy-outreach-and-follow-up-sdr)
     1. [Preparing for Field Events](/handbook/marketing/revenue-marketing/sdr/#preparing-for-field-events)
1. SDR to inform Sales of pending SAOs (in meeting and/or SFDC chatter)
1. Review status of pending IQM’s, qualifying leads, target accounts the Sales has provided the SDR, SDR outbounding activities, and marketing events. 
1. SDRs should always aim to attend the initial meeting (IQM) so they can introduce the prospect to the Sales team (when possible, Chorus notetaker should also be included). If for some reason the SDR can not join, this should be communicated to the Sales team in advance. The SDR will be responsible for:
     1. Prior to IQM, the SDR should share any Pre-IQM notes with SAL (for Large/Enterprise SDRs please share IQM Event links with SALs).
     Start of IQM: Greet all guests and explain Chorus. "Hi, I'm sure you see GitLab notetaker on this call. We use a tool called Chorus that aids us in note taking and for training purposes. Do you have a problem with our call being recorded? I can always turn the recording off."
     1. The SDR should begin the meeting by recapping what they have discussed so far with the prospect, reviewing proposed agenda items that were included in the calendar invite and introducing the Sales Executive who will dive deeper into qualification/discovery. 
     1. SDR assists with making sure the last 5 minutes of the meeting is reserved for setting up next steps.
     1. SDR responsible for updating opportunity in SFDC with notes from call and chattering Sales Executive on opportunity for either advancement or closure. 

#### Account Prioritization 

1. Sales to work with SDR on target accounts for strategic outbound prospecting purposes.
     1. Discuss “wide” vs “deep” net approach and agree on a good capacity for SDR to begin with. Typically, new SDR’s start with around 3-5 accounts per Sales Executive and add additional from there depending on personal bandwidth. Targeted ICP contact lists are pulled for each outreach sequence that the SDR creates.
     1. Sales Executives share a list of accounts in their territory with SDR. They should review and prioritize a “top 10” list and discuss which accounts SDR should start outreach to first. Prioritization categories include but are not limited to: CE Users (version.gitlab.com), Industry Vertical, Executive/Board Connections, Current marketing engagement with account, etc. 
     1. Since most SDR/Sales Team pairings have 2:1 or 3:1 mappings, SDR’s should discuss their plan for equal attention/work load for each Sales Executive, as well as how many accounts they’d like to initially start with..

### EMEA Mid-Market SDR to AE Outbound IQM Handoffs

The purpose of the SDR-AE outbound handoff process is three fold.

1. Ensure Outbound prospects get the best experience possible.
1. Ensure the SDR team enjoys operational efficiencies and have clearly structured ways to collaborate with the AE team on outbound accounts.
1. Ensure the AE team is set up for success is receiving outbound leads in a structured manner and have the appropriate information to utilize in converting them. 

To make sure that the hand-offs maximize internal efficiencies, it is required that:
- The SDR team makes sure to book calls, with a minimum notice time of 48 business hours.
- The SDR team makes sure to properly fulfil Outbound SAO criteria, as per the guidelines below for Warm IQMs.
- The SDR team makes sure to articulate:
     - Timeline and Procurement Process for a Pricing Call
     - Quantifiable Business Pains and Business Goals for Cold IQMs. 
- The AE team makes sure to: 
     - Accept SAOs immediately after a Warm IQM call.
     - To Accept SAOs immediately after the Cold IQM/Pricing Call email has been received.
     - To leave a chatter note on the opportunity record, tagging SDR and AE manager with feedback on the level of qualification and handoff in case of discrepancies.  
     - To be responsible for managing the prospect relationship after all handoff types have taken place. This includes re-scheduling conflicts.

### Warm Initial Qualifying Meeting (Warm IQM)

#### External Lingo: Evaluation Orchestration Call

- Are leads that have been qualified by the SDR over a Discovery call. 
- CoM principles have been applied to the call and some of the Before/After Scenarios, Positive Business Outcomes, Requirements and Metrics have been identified and agreed upon between the prospect and the SDR. 
- There is a clear need or intent identified from the side of the company, and the prospect has clearly articulated a path to the ultimate decision-maker, as per the guidelines for outbound [SAO criteria](https://docs.google.com/document/d/1m5YBOCc--M1Iq5-SEEd2OUWDjYyc6VJ3xTsDEEqisUQ/edit)

#### SDR steps after discovery call

- Summarize CoM principles uncovered during the call
- Schedule next step [through Outreach](https://app1a.outreach.io/meetings) while being on the call with the prospect
    - Meeting Type should be 45’ Evaluation Orchestration Call and body of invitation should be adjusted to meet the prospect’s needs.
- Send [Warm AE Intro Email](https://app1a.outreach.io/templates/50415)
    - For demanding hand-offs, [customer-facing](https://docs.google.com/document/d/1EpltUVDhIbgWLcGXD_ug8PkoPOSBxKkNiwpwy7irbsw/edit) agenda may also be copied and attached to intro email. 
- Log Required SFDC fields and populate Notes field [as per the guidelines here](https://docs.google.com/document/d/1m5YBOCc--M1Iq5-SEEd2OUWDjYyc6VJ3xTsDEEqisUQ/edit)
- **Unless there's a scheduling conflict for the SDR**, attend Evaluation Orchestration Call and kick-off the call: 
     - Summarize the SDR qualifying conversation by mentioning and verifying the before and after scenario of the prospect, as transcribed on the SAO notes. 
     - After prospect acknowledges that their internal situation have not changes inbetween SDR and AE call, recap the expectations and metrics as transcribed on the SAO notes and handoff to AE to move forward.

### Cold Initial Qualifying Meeting (Cold IQM)

#### External Lingo: Evaluation Orchestration Call

Are meetings for leads that the SDR is certain are a qualified opportunity but cannot get them on a qualifying call himself OR the AE has agreed to work on directly themselves
This applies to leads that are unresponsive or have strictly stated that they do not want to have an introductory call with the SDR 
In these cases, the SDRs can push for an IQM with the AE directly over email


### SDR steps on email thread

- While on email thread, [send pre-IQM email to gather preliminary information.](https://app1a.outreach.io/snippets/635)
- Summarize Prospect’s pain points on [Cold AE Intro Email](https://app1a.outreach.io/templates/50416) and send.
- If required by prospect, schedule next step [through Outreach.](https://app1a.outreach.io/meetings)
     - Meeting Type should be 30’ IQM
- Log Required SFDC fields and populate Notes field [as per the guidelines here.](https://docs.google.com/document/d/1am2y4pYhinZ4oVDdt3NWay1i27doi9lixUIWBjpuSoM/edit)
 
### Quarterly Business Reviews

It is highly encouraged for Sales to involve their SDR in QBR prep, planning as well as invite them to attend and speak during their presentation. SDR’s should carve out time to assist and contribute with Sales presentation slides.
