---
layout: markdown_page
title: "Maintenance First (to be renamed)"
description: "Transition R&D teams into a steady priorization state driven by backlog data, SLO guidelines & healthy prioritization ratios"
canonical_path: "/company/team/structure/working-groups/steady-prioritization/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Attributes

| Property        | Value           |
|-----------------|-----------------|
| Date Created    | 2022-04-07 |
| Target End Date | 2022-05-31  |
| Slack           | [#wg_maint-first](https://gitlab.slack.com/archives/C03AWM7780G) |
| Google Doc      | [Working Group Agenda](https://docs.google.com/document/d/1wog8bR7jg6SECefx9BGqIa07sFm_sXJPvelVAganYmc/edit#heading=h.pmtw3ocv2aty)  |
| Task Board      | [Issue board](https://gitlab.com/groups/gitlab-com/-/epic_boards/20810) |
| Epic            | [Link](https://gitlab.com/groups/gitlab-com/-/epics/1799) |

## Business Goal

Transition R&D teams into a steady prioritization state driven by backlog data, SLO guidelines & healthy prioritization ratios. We would like to transition R&D teams to a healthy and stable prioritization state after the complete burn-down of their Reliability & Security debt backlog (estimate all teams by the end of May). [Type improvements & initial dashboards have been completed prior to the working group.](https://gitlab.com/groups/gitlab-com/-/epics/1799#pre-work-by-april-15th) 



### Exit Criteria (0% completed)

1. Company-wide comms by April 30th
1. Complete stakeholder dashboard views, the backlog of bugs, and maintenance types.
1. Attain less than 5% undefined MR types 
1. Transition x% of product group teams into Steady prioritization state
1. Setup monthly MR type reviews between engineering managers
1. Move dashboards for teams into their respective handbook pages
1. Bot automation of auto-scheduling bug & maintenance types 
1. Set goal of Development department-wide steady prioritization state (60/30/10)
1. Other handbook cleanup


### Roles and Responsibilities

| Working Group Role    | Person                                               | Title                                                      |
|-----------------------|------------------------------------------------------|------------------------------------------------------------|
| Executive Sponsor     | Eric Johnson                                         | CTO                                                        |
| Facilitator           | Wayne Haber                                          | Director of Engineering                                    |
| Functional Lead       | Christopher Lefelhocz                                | VP of Development                                          | 
| Functional Lead       | Christie Lenneville                                  | VP of UX  |
| Functional Lead       | Mek Stittri                                          | VP of Quality |
| Functional Lead       | David DeSanto                                        | VP of Product Management |
| Functional Lead       | Justin Farris                                        | Senior Director of Product Management |
| Member                | Lily Mai                                             | Staff Engineering Analyst |
| Member                | Tanya Pazitny                                        | Director of Quality Engineering |
| Member                | John Hope                                            | Engineering Manager, Plan:Product Planning & Certify |
| Member                | Matt Wilson                                          | Senior Product Manager, Secure |
      
